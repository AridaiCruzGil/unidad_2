import java.util.List;

public class FP_Functional_Exercises{
    
    public static void main(String[] args){
      List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
      
      List<String> courses = List.of("Spring", "Spring Boot", "API",
         "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
         
      System.out.println("\nExercise 1 --> Print Only Odd Numbers from the List");
         numbers.stream()                                      //Convert to Stream
                .filter(number -> number % 2 !=0)                     //Lamdba Expression
                .forEach(FP_Functional_Exercises::print);     // Method Reference
      System.out.println("");
        
      System.out.println("\nExercise 2 --> Print All Courses individually");
      courses.stream().forEach(FP_Functional_Exercises::print);
      System.out.println("");
       
      System.out.println("\nExercise 3 --> Print Courses Containing the word \"Spring\"");
      courses.stream().filter(course -> course.contains("Spring")).forEach(FP_Functional_Exercises::print); 
      System.out.println(""); 
      
      System.out.println("\nExercise 4 --> Print Courses Whose Name has atleast 4 letters");
      courses.stream().filter(course -> course.length() >= 4).forEach(FP_Functional_Exercises::print);
      System.out.println("");    
      
      System.out.println("\nExercise 5 --> Print the cubes of Odd numbers");
      numbers.stream().filter(number -> number % 2 != 0)
                      .map (number -> number * number * number).forEach(FP_Functional_Exercises::print);
      System.out.println("");  
      
      System.out.println("\nExercise 5 --> Print the number of Characters in each course name");
      courses.stream().map(course -> course + " = " + course.length()).forEach(FP_Functional_Exercises::print);
       System.out.println("");  
       
       System.out.println("\n");         
       }
       
       private static void print(int number){
       System.out.print(number + ", ");
       }
       
       private static void print(String course){
       System.out.print(course + ", ");
       }
     }
       
       
       
             